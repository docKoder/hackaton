package pt.academiadecodigo.cesar.Entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import pt.academiadecodigo.cesar.MyMasterGame;
import pt.academiadecodigo.cesar.TextureManager;
import pt.academiadecodigo.cesar.screen.ScreenManager;

public class Enemy extends Entity {

    public Enemy(Vector2 pos, Vector2 direction) {
        super(TextureManager.ENEMY, pos, direction);
    }

    @Override
    public void update() {
        pos.add(direction);

        if (pos.y <= -TextureManager.ENEMY.getHeight()) {
            float x = MathUtils.random(0, MyMasterGame.WIDTH - TextureManager.ENEMY.getWidth());
            pos.set(x,MyMasterGame.HEIGHT );
        }
    }
}
