package pt.academiadecodigo.cesar.Entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import pt.academiadecodigo.cesar.MyMasterGame;
import pt.academiadecodigo.cesar.TextureManager;

public class Missile extends Entity {

    public Missile(Vector2 pos) {
        super(TextureManager.ENEMY, pos, new Vector2(0, 5));
    }

    @Override
    public void update() {
        pos.add(direction);
    }

    public boolean checkEnd(){
        return pos.y >= MyMasterGame.HEIGHT;
    }
}
