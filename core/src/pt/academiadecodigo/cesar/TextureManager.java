package pt.academiadecodigo.cesar;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class TextureManager {

    public static Texture PLAYER = new Texture(Gdx.files.internal("badlogic.jpg"));
    public static Texture MISSLE = new Texture(Gdx.files.internal("badlogic.jpg"));
    public static Texture ENEMY = new Texture(Gdx.files.internal("badlogic.jpg"));

}
