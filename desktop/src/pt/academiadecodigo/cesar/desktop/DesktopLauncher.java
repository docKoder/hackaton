package pt.academiadecodigo.cesar.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import pt.academiadecodigo.cesar.MyMasterGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = MyMasterGame.WIDTH;
		config.height = MyMasterGame.HEIGHT;
		config.title = "Space Invaderers";
		//config.useGL20 = true;
		new LwjglApplication(new MyMasterGame(), config);
	}
}
